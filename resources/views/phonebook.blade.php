<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    <link href="{{asset("css/app.css")}}" rel="stylesheet" type="text/css">

  </head>
  <body>

    <div id="app">
      <header-component></header-component>

      <div class="container">
          <router-view></router-view>
      </div>


      <footer-component></footer-component>

    </div>

    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
  </body>
</html>
